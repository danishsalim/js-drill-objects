function pairs(obj) {
  if (typeof obj == "object") {
    //checking if obj is object or not
    let arr = [];
    for (let key in obj) {
      //pushin key val pair in 2d array form.
      arr.push([key, obj[key]]);
    }
    return arr;
  } else {
    return;
  }
}

export default pairs;
