function defaults(obj, defaultProps) {
  if (typeof obj == "object") {
    //checking if obj is object or not
    for (let key in defaultProps) {
      if (!obj[key]) {
        //if key of obj is not present in default then we have to add it
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  } else {
    return;
  }
}

export default defaults;
