function mapObject(obj, cb) {
  const mappedObject = {};
  if (typeof obj == "object") {
    //checking if obj is object or not
    for (let key in obj) {
      //passing all element to cb function and pushing the returned val to mappedObject
      mappedObject[key] = cb(obj[key], key);
    }
    return mappedObject;
  } else {
    return;
  }
}

export default mapObject;
