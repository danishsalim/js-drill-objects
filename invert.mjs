function invert(obj) {
  if (typeof obj == "object") {
    //checking if obj is object or not
    let invertObject = {};
    for (let key in obj) {
      //inverting key values
      invertObject[obj[key]] = key;
    }
    return invertObject;
  } else {
    return;
  }
}

export default invert;
