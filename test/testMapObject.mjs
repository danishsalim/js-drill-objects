import testObject from "../testObject.mjs";
import mapObject from "../mapObject.mjs";

const mappedObject = mapObject(testObject, (val, key) => {
  return val + " BatMan";
});

console.log(mappedObject);
