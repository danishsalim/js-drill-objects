function values(obj) {
  if (typeof obj == "object") {
    //checking if obj is object or not
    let arr = [];
    for (let key in obj) {
      //pushing all values into array
      arr.push(obj[key]);
    }
    return arr;
  } else {
    return;
  }
}

export default values;
